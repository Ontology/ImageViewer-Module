﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageViewer_Module
{
    public class ThumbNailImage
    {
        public bool ThumbnailCallback()
        {
            return false;
        }
        public Image GetThumbNailOfImage(Image image, int width, int height)
        {
            Image.GetThumbnailImageAbort myCallback =
            new Image.GetThumbnailImageAbort(ThumbnailCallback);
            Image myThumbnail = image.GetThumbnailImage(
            width, height, myCallback, IntPtr.Zero);
            return myThumbnail;
        }
    }
}
