﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MediaStore_Module;
using ImageViewer_Module.ViewModels;

namespace ImageViewer_Module
{
    public class DataCollector : ViewModelBase
    {


        private clsLocalConfig localConfig;

        private MediaStoreConnector mediaStoreConnector;

        private string imagePath = null;
        public string ImagePath
        {
            get
            {
                if (string.IsNullOrEmpty(imagePath))
                {
                    imagePath = Environment.ExpandEnvironmentVariables(@"%TEMP%\Images");
                }
                return imagePath;
            }
        }

        private OntologyModDBConnector dbReader_Images;
        private OntologyModDBConnector dbReader_Files;

        private clsOntologyItem refItem;

        private List<ImageItem> imageList;
        public List<ImageItem> ImageList
        {
            get { return imageList; }
            set
            {
                imageList = value;
                RaiseChangeNotification(NotifyChanges.DataCollector_FileList);
            }
        }

        private clsOntologyItem result_Load;
        public clsOntologyItem Result_Load
        {
            get { return result_Load; }
            set
            {
                result_Load = value;
                RaiseChangeNotification(NotifyChanges.DataCollector_Result_Load);
            }
        }

        public void LoadImagesOfRef(clsOntologyItem refItem)
        {
            this.refItem = refItem;

            var result = GetData_001_Images();
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = GetData_002_Files();
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var index = 0;
                ImageList = (from refToImage in dbReader_Images.ObjectRels
                            join imageToFile in dbReader_Files.ObjectRels on refToImage.ID_Object equals imageToFile.ID_Object
                            orderby refToImage.OrderID, refToImage.Name_Object
                            select new ImageItem(localConfig, mediaStoreConnector)
                            {
                                IdImage = refToImage.ID_Object,
                                NameImage = refToImage.Name_Object,
                                IdFile = imageToFile.ID_Other,
                                NameFile = imageToFile.Name_Other,
                                PathFile = ImagePath + Path.DirectorySeparatorChar + imageToFile.Name_Other,
                                IdRef = refToImage.ID_Other,
                                NameRef = refToImage.Name_Other
                            }).ToList();
            }

            Result_Load = result;
        }

        private clsOntologyItem GetData_001_Images()
        {
            var searchImages = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = refItem.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belongs_to.GUID,
                    ID_Parent_Object = localConfig.OItem_class_images__graphic_.GUID
                }
            };

            var result = dbReader_Images.GetDataObjectRel(searchImages);

            return result;
        }

        private clsOntologyItem GetData_002_Files()
        {
            var searchFiles = dbReader_Images.ObjectRels.Select(imageRelItem => new clsObjectRel
            {
                ID_Object = imageRelItem.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                ID_Parent_Other = localConfig.OItem_class_file.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchFiles.Any())
            {
                result = dbReader_Files.GetDataObjectRel(searchFiles);
            }
            else
            {
                dbReader_Files.ObjectRels.Clear();
            }
            

            return result;
        }

        public DataCollector(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            mediaStoreConnector = new MediaStoreConnector(localConfig.Globals);
            dbReader_Images = new OntologyModDBConnector(localConfig.Globals);
            dbReader_Files = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
