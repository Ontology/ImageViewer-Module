﻿using ImageViewer_Module.Converter;
using ImageViewer_Module.ViewModels;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageViewer_Module
{
    public class ViewerViewModel : ViewModelBase
    {
        private string text_Viewer;
        public string Text_Viewer
        {
            get { return text_Viewer; }
            set
            {
                text_Viewer = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Text_Viewer);
            }
        }

        private int progress_ImageLoad;
        public int Progress_ImageLoad
        {
            get { return progress_ImageLoad; }
            set
            {
                progress_ImageLoad = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Progress_ImageLoad);
            }
        }

        private List<ImageItem> imageItems;
        public List<ImageItem> ImageItems
        {
            get { return imageItems; }
            set
            {
                imageItems = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_ImageItems);
            }
        }

        private clsOntologyItem referenceItem;
        public clsOntologyItem ReferenceItem
        {
            get { return referenceItem; }
            set
            {
                referenceItem = value;
                if (referenceItem != null)
                {
                    IsEnabled_Edit = true;
                    Text_Viewer = referenceItem.Name;
                }
                else
                {
                    IsEnabled_Edit = false;
                }
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_ReferenceItem);
            }
        }

        private FormMessage formMessage;
        public FormMessage FormMessage
        {
            get { return formMessage; }
            set
            {
                formMessage = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_FormMessage);
            }
        }

        private int pageId;
        public int PageId
        {
            get { return pageId; }
            set
            {
                pageId = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_PageId);
            }
        }

        private int pageCount;
        public int PageCount
        {
            get { return pageCount; }
            set
            {
                pageCount = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_PageCount);
            }
        }

        public int pageRange;
        public int PageRange
        {
            get { return pageRange; }
            set
            {
                pageRange = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_PageRange);
            }
        }

        private string text_HeightLbl;
        public string Text_HeightLbl
        {
            get { return text_HeightLbl; }
            set
            {
                text_HeightLbl = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Text_HeightLbl);
            }
        }

        private string text_WidthLbl;
        public string Text_WidthLbl
        {
            get { return text_WidthLbl; }
            set
            {
                text_WidthLbl = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Text_WidthLbl);
            }
        }

        internal string text_Height;
        public string Text_Height
        {
            get { return text_Height; }
            set
            {
                text_Height = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Text_Height);
            }
        }

        internal string text_Width;
        public string Text_Width
        {
            get { return text_Width; }
            set
            {
                text_Width = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Text_Width);
            }
        }


        internal int height;
        public int Height
        {
            get { return height; }
            set
            {
                height = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Height);
            }
        }

        internal int width;
        public int Width
        {
            get { return width; }
            set
            {
                width = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Width);
            }
        }

        private bool isEnabled_NavFirst;
        public bool IsEnabled_NavFirst
        {
            get { return isEnabled_NavFirst; }
            set
            {
                isEnabled_NavFirst = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_IsEnabled_NavFirst);
            }
        }

        private bool isEnabled_NavPrevious;
        public bool IsEnabled_NavPrevious
        {
            get { return isEnabled_NavPrevious; }
            set
            {
                isEnabled_NavPrevious = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_IsEnabled_NavPrevious);
            }
        }

        private bool isEnabled_NavNext;
        public bool IsEnabled_NavNext
        {
            get { return isEnabled_NavNext; }
            set
            {
                isEnabled_NavNext = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_IsEnabled_NavNext);
            }
        }

        private bool isEnabled_NavLast;
        public bool IsEnabled_NavLast
        {
            get { return isEnabled_NavLast; }
            set
            {
                isEnabled_NavLast = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_IsEnabled_NavLast);
            }
        }

        public string Text_PageNav
        {
            get
            {
                return "Page " + (PageId + 1).ToString() + "/" + (PageCount).ToString() + 
                    "(" + (IxPageStart + 1).ToString()  + "-" + (IxPageLast + 1).ToString() + ")";
            }
        }

        private string text_PageRangeLbl;
        public string Text_PageRangeLbl
        {
            get { return text_PageRangeLbl; }
            set
            {
                text_PageRangeLbl = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Text_PageRangeLbl);
            }
        }

        private string text_All;
        public string Text_All
        {
            get { return text_All; }
            set
            {
                text_All = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Text_All);
            }
        }

        private int ixPageStart;
        public int IxPageStart
        {
            get { return ixPageStart; }
            set
            {
                ixPageStart = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_IxPageStart);
            }
        }

        private int ixPageLast;
        public int IxPageLast
        {
            get { return ixPageLast; }
            set
            {
                ixPageLast = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_IxPageLast);
            }
        }

        private string text_PageRange;
        public string Text_PageRange
        {
            get { return text_PageRange; }
            set
            {
                text_PageRange = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Text_PageRange);
            }
        }

        private bool isEnabled_ToClipboard;
        public bool IsEnabled_ToClipboard
        {
            get
            {
                return isEnabled_ToClipboard;
                
            }
            set
            {
                isEnabled_ToClipboard = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_IsEnabled_ToClipboard);
            }
        }

        private string text_ToClipboard;
        public string Text_ToClipboard
        {
            get { return text_ToClipboard; }
            set
            {
                text_ToClipboard = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Text_ToClipboard);
            }
        }

        private bool isEnabled_Edit;
        public bool IsEnabled_Edit
        {
            get { return isEnabled_Edit; }
            set
            {
                isEnabled_Edit = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_IsEnabled_Edit);
            }
        }

        private string text_Edit;
        public string Text_Edit
        {
            get { return text_Edit; }
            set
            {
                text_Edit = value;
                RaiseChangeNotification(NotifyChanges.ViewerViewModel_Text_Edit);
            }
        }
    }
}
