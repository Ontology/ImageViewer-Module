﻿using MediaStore_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageViewer_Module
{
    public class ImageItem 
    {

        private clsLocalConfig localConfig;
        private MediaStoreConnector storeConnector;
        
        private string idImage;
        public string IdImage
        {
            get { return idImage; }
            set
            {
                idImage = value;
                ManageOImageItem();
            }
        }

        private string nameImage;
        public string NameImage
        {
            get { return nameImage; }
            set
            {
                nameImage = value;
                ManageOImageItem();
            }
        }

        private string idFile;
        public string IdFile
        {
            get { return idFile; }
            set
            {
                idFile = value;
                ManageFileItem();
            }
        }

        private string nameFile;
        public string NameFile
        {
            get { return nameFile; }
            set
            {
                nameFile = value;
                ManageFileItem();
            }
        }

        private string pathFile;
        public string PathFile
        {
            get { return pathFile; }
            set
            {
                var oldPath = pathFile;
                pathFile = value;

            }
        }

        
        public string IdRef { get; set; }
        public string NameRef { get; set; }


        private void ManageFileItem()
        {
            if (fileItem == null)
            {
                fileItem = new clsOntologyItem
                {
                    GUID_Parent = localConfig.OItem_class_file.GUID,
                    Type = localConfig.Globals.Type_Object
                };
                
            }

            var oldGuid = fileItem.GUID;
            fileItem.GUID = IdFile;
            fileItem.Name = NameFile;
            
        }

        private void ManageOImageItem()
        {
            if (oItemImage == null)
            {
                oItemImage = new clsOntologyItem
                {
                    GUID_Parent = localConfig.OItem_class_images__graphic_.GUID,
                    Type = localConfig.Globals.Type_Object
                };

            }


            oItemImage.GUID = idImage;
            oItemImage.Name = nameImage;
        }
        private clsOntologyItem fileItem;
        public clsOntologyItem FileItem
        {
            get
            {
                return fileItem;
                
            }
            
        }

        private clsOntologyItem oItemImage;
        public clsOntologyItem OItemImage
        {
            get
            {
                return oItemImage;

            }

        }

        private clsOntologyItem resultLoad;
        public clsOntologyItem ResultLoad
        {
            get
            {
                return resultLoad;
            }
            private set
            {
                resultLoad = value;
            }
        }

        public void SaveImage()
        {
            if (FileItem == null || string.IsNullOrEmpty(pathFile)) return;

            var result = localConfig.Globals.LState_Success.Clone();

            if (!System.IO.File.Exists(pathFile))
            {
                result = storeConnector.SaveManagedMediaToFile(FileItem, pathFile, true);
            }


            resultLoad = result;
        }

        //private Image image;
        //public Image ImageObject
        //{
        //    get { return image; }
        //    private set
        //    {
        //        image = value;
        //    }
        //}


        public ImageItem(clsLocalConfig localConfig, MediaStoreConnector storeConnector)
        {
            this.localConfig = localConfig;
            
            this.storeConnector = storeConnector;
            ResultLoad = localConfig.Globals.LState_Nothing.Clone();
        }
    }
}
