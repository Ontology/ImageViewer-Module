﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageViewer_Module.Converter
{
    public static class ListViewHelper
    {
        public static int LastIndexOfDoubleClick { get; set; }
        private static ThumbNailImage thumbNailImage = new ThumbNailImage();

        public static clsOntologyItem IntegrateImagesToListView(ImageList smallList, ImageList bigList, List<ImageItem> imageList, clsLocalConfig localConfig, ListView listView, int width, int height)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            listView.Clear();
            smallList.Images.Clear();
            bigList.Images.Clear();
            var ix = 0;
            foreach(var imageItem in imageList)
            {
                imageItem.SaveImage();
                if (imageItem.ResultLoad.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var image = Image.FromFile(imageItem.PathFile);
                    //var thumbSmall = thumbNailImage.GetThumbNailOfImage(image, width, height);
                    //var thumbBig = thumbNailImage.GetThumbNailOfImage(image, width, height);
                    var thumb = new Bitmap(width, height);
                    using (var g = Graphics.FromImage(thumb))
                    {
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        g.DrawImage(image, new Rectangle(0, 0, width, height));
                    }
                    image.Dispose();

                    smallList.Images.Add(thumb);
                    bigList.Images.Add(thumb);

                }
                else
                {
                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        result = localConfig.Globals.LState_Error.Clone();
                    }
                    result.Count++;

                    smallList.Images.Add(new Bitmap(128, 128, PixelFormat.Format32bppArgb));
                    bigList.Images.Add(new Bitmap(128, 128, PixelFormat.Format32bppArgb));
                }
                var listViewItem = new ListViewItem();
                listViewItem.Name = imageItem.IdImage;
                listViewItem.Text = imageItem.NameImage;
                listViewItem.ImageIndex = ix;
                listView.Items.Add(listViewItem);
                ix++;
            }

            return result;
        }

        public static bool ThumbnailCallback()
        {
            return false;
        }

        public static List<clsOntologyItem> GetObjectListForObjectEdit(ListView listView, List<ImageItem> imageItemList, MouseEventArgs e)
        {

            List<clsOntologyItem> objectList = null;
            LastIndexOfDoubleClick = 0;
            var clickedItem = listView.HitTest(e.Location).Item;
            if (clickedItem != null)
            {
                LastIndexOfDoubleClick = clickedItem.Index;
                objectList = imageItemList.Select(imageItem => imageItem.OItemImage).ToList();

            }

            return objectList;
        }

    }
}
