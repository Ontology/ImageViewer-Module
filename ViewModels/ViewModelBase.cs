﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageViewer_Module.ViewModels
{
    public delegate void ChangeNotify(string nameProperty);
    public class ViewModelBase
    {
        
        public event ChangeNotify changeNotify;

        public void RaiseChangeNotification(string propertyName)
        {
            if (changeNotify != null)
            {
                changeNotify(propertyName);
            }
        }
    }
}
