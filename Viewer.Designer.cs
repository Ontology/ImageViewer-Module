﻿namespace ImageViewer_Module
{
    partial class Viewer
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Viewer));
            this.imageList_Big = new System.Windows.Forms.ImageList(this.components);
            this.imageList_Small = new System.Windows.Forms.ImageList(this.components);
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_NavFirst = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_NavPrevious = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Next = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Last = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel_PageNav = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripProgressBar_ImageLoad = new System.Windows.Forms.ToolStripProgressBar();
            this.listView_Images = new System.Windows.Forms.ListView();
            this.contextMenuStrip_Items = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toOntologyClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel_PageRange = new System.Windows.Forms.ToolStripLabel();
            this.toolStripDropDownButton_PageRange = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem_10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_50 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_100 = new System.Windows.Forms.ToolStripMenuItem();
            this.allToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel_Height = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBox_Height = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel_Width = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBox_Width = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Edit = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip_Items.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList_Big
            // 
            this.imageList_Big.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList_Big.ImageSize = new System.Drawing.Size(256, 256);
            this.imageList_Big.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // imageList_Small
            // 
            this.imageList_Small.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList_Small.ImageSize = new System.Drawing.Size(128, 128);
            this.imageList_Small.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.listView_Images);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1078, 401);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1078, 451);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip2);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_NavFirst,
            this.toolStripButton_NavPrevious,
            this.toolStripButton_Next,
            this.toolStripButton_Last,
            this.toolStripLabel_PageNav,
            this.toolStripSeparator1,
            this.toolStripProgressBar_ImageLoad});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.ShowItemToolTips = false;
            this.toolStrip1.Size = new System.Drawing.Size(265, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // toolStripButton_NavFirst
            // 
            this.toolStripButton_NavFirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_NavFirst.Image = global::ImageViewer_Module.Properties.Resources.pulsante_01_architetto_f_01_First1;
            this.toolStripButton_NavFirst.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_NavFirst.Name = "toolStripButton_NavFirst";
            this.toolStripButton_NavFirst.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_NavFirst.Text = "toolStripButton1";
            this.toolStripButton_NavFirst.Click += new System.EventHandler(this.toolStripButton_NavFirst_Click);
            // 
            // toolStripButton_NavPrevious
            // 
            this.toolStripButton_NavPrevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_NavPrevious.Image = global::ImageViewer_Module.Properties.Resources.pulsante_01_architetto_f_01;
            this.toolStripButton_NavPrevious.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_NavPrevious.Name = "toolStripButton_NavPrevious";
            this.toolStripButton_NavPrevious.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_NavPrevious.Text = "toolStripButton2";
            this.toolStripButton_NavPrevious.Click += new System.EventHandler(this.toolStripButton_NavPrevious_Click);
            // 
            // toolStripButton_Next
            // 
            this.toolStripButton_Next.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Next.Image = global::ImageViewer_Module.Properties.Resources.pulsante_02_architetto_f_01;
            this.toolStripButton_Next.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Next.Name = "toolStripButton_Next";
            this.toolStripButton_Next.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Next.Text = "toolStripButton3";
            this.toolStripButton_Next.Click += new System.EventHandler(this.toolStripButton_Next_Click);
            // 
            // toolStripButton_Last
            // 
            this.toolStripButton_Last.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Last.Image = global::ImageViewer_Module.Properties.Resources.pulsante_02_architetto_f_01_Last;
            this.toolStripButton_Last.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Last.Name = "toolStripButton_Last";
            this.toolStripButton_Last.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Last.Text = "toolStripButton4";
            this.toolStripButton_Last.Click += new System.EventHandler(this.toolStripButton_Last_Click);
            // 
            // toolStripLabel_PageNav
            // 
            this.toolStripLabel_PageNav.Name = "toolStripLabel_PageNav";
            this.toolStripLabel_PageNav.Size = new System.Drawing.Size(53, 22);
            this.toolStripLabel_PageNav.Text = "Page 0/0";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripProgressBar_ImageLoad
            // 
            this.toolStripProgressBar_ImageLoad.Name = "toolStripProgressBar_ImageLoad";
            this.toolStripProgressBar_ImageLoad.Size = new System.Drawing.Size(100, 22);
            // 
            // listView_Images
            // 
            this.listView_Images.ContextMenuStrip = this.contextMenuStrip_Items;
            this.listView_Images.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_Images.LargeImageList = this.imageList_Big;
            this.listView_Images.Location = new System.Drawing.Point(0, 0);
            this.listView_Images.Name = "listView_Images";
            this.listView_Images.Size = new System.Drawing.Size(1078, 401);
            this.listView_Images.SmallImageList = this.imageList_Small;
            this.listView_Images.TabIndex = 2;
            this.listView_Images.UseCompatibleStateImageBehavior = false;
            this.listView_Images.View = System.Windows.Forms.View.Tile;
            this.listView_Images.SelectedIndexChanged += new System.EventHandler(this.listView_Images_SelectedIndexChanged);
            this.listView_Images.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView_Images_MouseDoubleClick);
            // 
            // contextMenuStrip_Items
            // 
            this.contextMenuStrip_Items.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toOntologyClipboardToolStripMenuItem});
            this.contextMenuStrip_Items.Name = "contextMenuStrip_Items";
            this.contextMenuStrip_Items.Size = new System.Drawing.Size(198, 26);
            // 
            // toOntologyClipboardToolStripMenuItem
            // 
            this.toOntologyClipboardToolStripMenuItem.Name = "toOntologyClipboardToolStripMenuItem";
            this.toOntologyClipboardToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.toOntologyClipboardToolStripMenuItem.Text = "To Ontology-Clipboard";
            this.toOntologyClipboardToolStripMenuItem.Click += new System.EventHandler(this.toOntologyClipboardToolStripMenuItem_Click);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel_PageRange,
            this.toolStripDropDownButton_PageRange,
            this.toolStripSeparator2,
            this.toolStripLabel_Height,
            this.toolStripTextBox_Height,
            this.toolStripLabel_Width,
            this.toolStripTextBox_Width,
            this.toolStripSeparator3,
            this.toolStripButton_Edit});
            this.toolStrip2.Location = new System.Drawing.Point(3, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(494, 25);
            this.toolStrip2.TabIndex = 0;
            // 
            // toolStripLabel_PageRange
            // 
            this.toolStripLabel_PageRange.Name = "toolStripLabel_PageRange";
            this.toolStripLabel_PageRange.Size = new System.Drawing.Size(84, 22);
            this.toolStripLabel_PageRange.Text = "x_Page-Range:";
            // 
            // toolStripDropDownButton_PageRange
            // 
            this.toolStripDropDownButton_PageRange.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton_PageRange.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_10,
            this.toolStripMenuItem_50,
            this.toolStripMenuItem_100,
            this.allToolStripMenuItem});
            this.toolStripDropDownButton_PageRange.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton_PageRange.Image")));
            this.toolStripDropDownButton_PageRange.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton_PageRange.Name = "toolStripDropDownButton_PageRange";
            this.toolStripDropDownButton_PageRange.Size = new System.Drawing.Size(32, 22);
            this.toolStripDropDownButton_PageRange.Text = "10";
            this.toolStripDropDownButton_PageRange.DropDownClosed += new System.EventHandler(this.toolStripDropDownButton_PageRange_DropDownClosed);
            // 
            // toolStripMenuItem_10
            // 
            this.toolStripMenuItem_10.Name = "toolStripMenuItem_10";
            this.toolStripMenuItem_10.Size = new System.Drawing.Size(92, 22);
            this.toolStripMenuItem_10.Text = "10";
            this.toolStripMenuItem_10.Click += new System.EventHandler(this.toolStripMenuItem_10_Click);
            // 
            // toolStripMenuItem_50
            // 
            this.toolStripMenuItem_50.Name = "toolStripMenuItem_50";
            this.toolStripMenuItem_50.Size = new System.Drawing.Size(92, 22);
            this.toolStripMenuItem_50.Text = "50";
            this.toolStripMenuItem_50.Click += new System.EventHandler(this.toolStripMenuItem_50_Click);
            // 
            // toolStripMenuItem_100
            // 
            this.toolStripMenuItem_100.Name = "toolStripMenuItem_100";
            this.toolStripMenuItem_100.Size = new System.Drawing.Size(92, 22);
            this.toolStripMenuItem_100.Text = "100";
            this.toolStripMenuItem_100.Click += new System.EventHandler(this.toolStripMenuItem_100_Click);
            // 
            // allToolStripMenuItem
            // 
            this.allToolStripMenuItem.Name = "allToolStripMenuItem";
            this.allToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.allToolStripMenuItem.Text = "All";
            this.allToolStripMenuItem.Click += new System.EventHandler(this.allToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel_Height
            // 
            this.toolStripLabel_Height.Name = "toolStripLabel_Height";
            this.toolStripLabel_Height.Size = new System.Drawing.Size(46, 22);
            this.toolStripLabel_Height.Text = "Height:";
            // 
            // toolStripTextBox_Height
            // 
            this.toolStripTextBox_Height.Name = "toolStripTextBox_Height";
            this.toolStripTextBox_Height.Size = new System.Drawing.Size(100, 25);
            this.toolStripTextBox_Height.Leave += new System.EventHandler(this.toolStripTextBox_Height_Leave);
            // 
            // toolStripLabel_Width
            // 
            this.toolStripLabel_Width.Name = "toolStripLabel_Width";
            this.toolStripLabel_Width.Size = new System.Drawing.Size(42, 22);
            this.toolStripLabel_Width.Text = "Width:";
            // 
            // toolStripTextBox_Width
            // 
            this.toolStripTextBox_Width.Name = "toolStripTextBox_Width";
            this.toolStripTextBox_Width.Size = new System.Drawing.Size(100, 25);
            this.toolStripTextBox_Width.Leave += new System.EventHandler(this.toolStripTextBox_Width_Leave);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_Edit
            // 
            this.toolStripButton_Edit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_Edit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Edit.Image")));
            this.toolStripButton_Edit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Edit.Name = "toolStripButton_Edit";
            this.toolStripButton_Edit.Size = new System.Drawing.Size(31, 22);
            this.toolStripButton_Edit.Text = "Edit";
            this.toolStripButton_Edit.Click += new System.EventHandler(this.toolStripButton_Edit_Click);
            // 
            // Viewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 451);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "Viewer";
            this.Text = "x_ImageViewer";
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip_Items.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageList_Small;
        private System.Windows.Forms.ImageList imageList_Big;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton_NavFirst;
        private System.Windows.Forms.ToolStripButton toolStripButton_NavPrevious;
        private System.Windows.Forms.ToolStripButton toolStripButton_Next;
        private System.Windows.Forms.ToolStripButton toolStripButton_Last;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar_ImageLoad;
        private System.Windows.Forms.ListView listView_Images;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_PageNav;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_PageRange;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton_PageRange;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_50;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_100;
        private System.Windows.Forms.ToolStripMenuItem allToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Height;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox_Height;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Width;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox_Width;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip_Items;
        private System.Windows.Forms.ToolStripMenuItem toOntologyClipboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton_Edit;
    }
}

