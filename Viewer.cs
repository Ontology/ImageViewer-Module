﻿using ImageViewer_Module.Converter;
using ImageViewer_Module.ViewModels;
using MediaStore_Module;
using Ontology_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageViewer_Module
{
    public partial class Viewer : Form
    {
        private ViewerViewController viewModelController;
        
        
        
        private frmMain frmMain;
        
        private frm_ObjectEdit frmObjectEdit;

        public Viewer()
        {
            InitializeComponent();
            viewModelController = new ViewerViewController();
            viewModelController.changeNotify += ViewModelController_changeNotify;
            viewModelController.getReferenceItem += ViewModelController_getReferenceItem;
            viewModelController.InitViewController();
        }

        private void ViewModelController_getReferenceItem()
        {
            if (this.InvokeRequired)
            {
                GetReferenceItem deleg = new GetReferenceItem(ViewModelController_getReferenceItem);
                this.Invoke(deleg);
            }
            else
            {
                frmMain = new frmMain(viewModelController.LocalConfig.Globals);
                if (frmMain.ShowDialog() == DialogResult.OK)
                {
                    if (frmMain.OList_Simple.Count == 1)
                    {
                        viewModelController.ReferenceItem = frmMain.OList_Simple.First();
                    }
                    else
                    {
                        viewModelController.GetReferenceProblem();
                    }
                }
            }
        }

        private void ViewModelController_changeNotify(string nameProperty)
        {
            if (this.InvokeRequired)
            {
                ChangeNotify deleg = new ChangeNotify(DataCollector_changeNotify);
                this.Invoke(deleg, nameProperty);
            }
            else
            {
                if (nameProperty == NotifyChanges.ViewerViewModel_Text_Viewer)
                {
                    this.Text = viewModelController.Text_Viewer;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_Progress_ImageLoad)
                {
                    toolStripProgressBar_ImageLoad.Value = viewModelController.Progress_ImageLoad;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_ImageItems)
                {
                    viewModelController.SetListSetResult(ListViewHelper.IntegrateImagesToListView(imageList_Small, 
                        imageList_Big, 
                        viewModelController.ImageItems, 
                        viewModelController.LocalConfig, 
                        listView_Images,
                        viewModelController.Width, 
                        viewModelController.Height));

                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_PageId 
                    || nameProperty == NotifyChanges.ViewerViewModel_PageCount
                    || nameProperty == NotifyChanges.ViewerViewModel_IxPageStart
                    || nameProperty == NotifyChanges.ViewerViewModel_IxPageLast)
                {
                    toolStripLabel_PageNav.Text = viewModelController.Text_PageNav;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_IsEnabled_NavFirst)
                {
                    toolStripButton_NavFirst.Enabled = viewModelController.IsEnabled_NavFirst;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_IsEnabled_NavPrevious)
                {
                    toolStripButton_NavPrevious.Enabled = viewModelController.IsEnabled_NavPrevious;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_IsEnabled_NavNext)
                {
                    toolStripButton_Next.Enabled = viewModelController.IsEnabled_NavNext;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_IsEnabled_NavLast)
                {
                    toolStripButton_Last.Enabled = viewModelController.IsEnabled_NavLast;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_Text_All)
                {
                    allToolStripMenuItem.Text = viewModelController.Text_All;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_Text_PageRange)
                {
                    toolStripDropDownButton_PageRange.Text = viewModelController.Text_PageRange;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_Width || nameProperty == NotifyChanges.ViewerViewModel_Height)
                {
                    if (viewModelController.Height <= 0 || viewModelController.Width <= 0) return;
                    imageList_Small.ImageSize = new Size(viewModelController.Width / 2, viewModelController.Height / 2);
                    imageList_Big.ImageSize = new Size(viewModelController.Width, viewModelController.Height);
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_Text_Width)
                {
                    toolStripTextBox_Width.ReadOnly = true;
                    toolStripTextBox_Width.Text = viewModelController.Text_Width;
                    toolStripTextBox_Width.ReadOnly = false;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_Text_Height)
                {
                    toolStripTextBox_Height.ReadOnly = true;
                    toolStripTextBox_Height.Text = viewModelController.Text_Height;
                    toolStripTextBox_Height.ReadOnly = false;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_IsEnabled_ToClipboard)
                {
                    toOntologyClipboardToolStripMenuItem.Enabled = viewModelController.IsEnabled_ToClipboard;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_IsEnabled_Edit)
                {
                    toolStripButton_Edit.Enabled = viewModelController.IsEnabled_Edit;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_Text_Edit)
                {
                    toolStripButton_Edit.Text = viewModelController.Text_Edit;
                }
                else if (nameProperty == NotifyChanges.ViewerViewModel_FormMessage)
                {
                    MessageBox.Show(this, viewModelController.FormMessage.Message,
                        viewModelController.FormMessage.Short,
                        FormMessageToButtonConverter.Convert(viewModelController.FormMessage),
                        FormMessageToIconConverter.Convert(viewModelController.FormMessage));
                }
            }
        }

        

        private void DataCollector_changeNotify(string nameProperty)
        {
            
        }

        

        private void listView_Images_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void listView_Images_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var listView = (ListView)sender;
            var objectList = ListViewHelper.GetObjectListForObjectEdit(listView, viewModelController.ImageItems, e);

            if (objectList != null)
            {
                frmObjectEdit = new frm_ObjectEdit(viewModelController.LocalConfig.Globals, objectList, ListViewHelper.LastIndexOfDoubleClick, viewModelController.LocalConfig.Globals.Type_Object, null);
                frmObjectEdit.ShowDialog(this);
            }
            


        }

        private void toolStripButton_NavFirst_Click(object sender, EventArgs e)
        {
            viewModelController.NavigateInList(NavType.First);
        }

        private void toolStripButton_NavPrevious_Click(object sender, EventArgs e)
        {
            viewModelController.NavigateInList(NavType.Previous);
        }

        private void toolStripButton_Next_Click(object sender, EventArgs e)
        {
            viewModelController.NavigateInList(NavType.Next);
        }

        private void toolStripButton_Last_Click(object sender, EventArgs e)
        {
            viewModelController.NavigateInList(NavType.Last);
        }

       
        private void toolStripDropDownButton_PageRange_DropDownClosed(object sender, EventArgs e)
        {
            
        }

        private void toolStripMenuItem_10_Click(object sender, EventArgs e)
        {
            SetPageRangeText(sender);
        }

        private void SetPageRangeText(object sender)
        {
            viewModelController.ChangedPageRange(((ToolStripMenuItem)sender).Text);
        }

        private void toolStripMenuItem_50_Click(object sender, EventArgs e)
        {
            SetPageRangeText(sender);
        }

        private void toolStripMenuItem_100_Click(object sender, EventArgs e)
        {
            SetPageRangeText(sender);
        }

        private void allToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetPageRangeText(sender);
        }

        private void toolStripTextBox_Height_Leave(object sender, EventArgs e)
        {
            if (!toolStripTextBox_Height.ReadOnly)
            {
                viewModelController.TryToSetHeight(toolStripTextBox_Height.Text);
            }
        }

        private void toolStripTextBox_Width_Leave(object sender, EventArgs e)
        {
            if (!toolStripTextBox_Width.ReadOnly)
            {
                viewModelController.TryToSetWidth(toolStripTextBox_Width.Text);
            }
        }

        private void listView_Images_SelectedIndexChanged(object sender, EventArgs e)
        {

            viewModelController.ChangedSelectionIndex(listView_Images.SelectedIndices);
        }

        private void toOntologyClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewModelController.CopySelectionToClipboard();
        }

        private void toolStripButton_Edit_Click(object sender, EventArgs e)
        {
            frmObjectEdit = new frm_ObjectEdit(viewModelController.LocalConfig.Globals, 
                new List<clsOntologyItem> { viewModelController.ReferenceItem }, 
                0, 
                viewModelController.LocalConfig.Globals.Type_Object, 
                null);
            frmObjectEdit.ShowDialog(this);
        }
    }
}
