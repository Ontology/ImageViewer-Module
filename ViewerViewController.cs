﻿using ImageViewer_Module.Converter;
using Ontology_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ImageViewer_Module
{
    public delegate void SelectedItem(clsOntologyItem oItemSelected);
    public delegate void GetReferenceItem();

    public enum NavType
    {
        First = 0,
        Previous = 1,
        Next = 2,
        Last = 4
    }
    public class ViewerViewController : ViewerViewModel
    {
        private clsArgumentParsing objArgumentParsing;

        private clsOntologyClipboard ontologyClipboard;

        public event GetReferenceItem getReferenceItem;
        public event SelectedItem selectedItem;

        private System.Windows.Forms.ListView.SelectedIndexCollection selectedIndexCollection;

        private clsLocalConfig localConfig;
        public clsLocalConfig LocalConfig
        {
            get { return localConfig; }
        }

        private DataCollector dataCollector;


        private void TranslateUi()
        {
            Text_PageRangeLbl = "Images per Page:";
            Text_All = "All";
            Text_HeightLbl = "Height:";
            Text_WidthLbl = "Width:";
            Text_Edit = "Edit";
            

        }
        public ViewerViewController()
        {
            


            PageRange = 10;

            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            this.changeNotify += ViewerViewController_changeNotify;
            Initialize();
        }

        private void Initialize()
        {
            ontologyClipboard = new clsOntologyClipboard(localConfig.Globals);
        }

        private void ViewerViewController_changeNotify(string nameProperty)
        {
            if (nameProperty == NotifyChanges.ViewerViewModel_ReferenceItem)
            {
                if (ReferenceItem != null)
                {
                    LoadImages();
                }
            }
            else if (nameProperty == NotifyChanges.ViewerViewModel_PageCount || nameProperty == NotifyChanges.ViewerViewModel_PageId)
            {
                ConfigureNavigation();
            }
            else if (nameProperty == NotifyChanges.ViewerViewModel_PageRange)
            {
                Text_PageRange = PageRange.ToString();
            }
            else if (nameProperty == NotifyChanges.ViewerViewModel_Height)
            {
                Text_Height = Height.ToString();
            }
            else if (nameProperty == NotifyChanges.ViewerViewModel_Width)
            {
                Text_Width = Width.ToString();
            }
        }

        private void ConfigureNavigation()
        {
            IsEnabled_NavFirst = false;
            IsEnabled_NavPrevious = false;
            IsEnabled_NavNext = false;
            IsEnabled_NavLast = false;

            if (PageId < PageCount-1)
            {
                IsEnabled_NavNext = true;
                IsEnabled_NavLast = true;
            }
            if (PageId > 0)
            {
                IsEnabled_NavFirst = true;
                IsEnabled_NavPrevious = true;
            }
        }

        public void InitViewController()
        {
            Height = 256;
            Width = 256;
            TranslateUi();
            ParseArguments();
            ConfigureNavigation();
            IsEnabled_ToClipboard = false;
            IsEnabled_Edit = ReferenceItem != null;
            if (ReferenceItem == null)
            {
                if (getReferenceItem != null)
                {
                    getReferenceItem();
                }
            }
        }

        private void LoadImages()
        {
            PageCount = 0;
            PageId = 0;
            dataCollector = new DataCollector(localConfig);
            dataCollector.changeNotify += DataCollector_changeNotify; ;
            if (!Directory.Exists(dataCollector.ImagePath))
            {
                Directory.CreateDirectory(dataCollector.ImagePath);
            }

            var directoryInfo = new DirectoryInfo(dataCollector.ImagePath);
            directoryInfo.GetFiles().ToList().ForEach(fileItem =>
            {
                fileItem.Delete();
            });
            Progress_ImageLoad = 50;
            dataCollector.LoadImagesOfRef(ReferenceItem);
        }

        private void DataCollector_changeNotify(string nameProperty)
        {
            if (nameProperty == NotifyChanges.DataCollector_Result_Load)
            {
                Progress_ImageLoad = 0;
                if (dataCollector.Result_Load.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    if (dataCollector.ImageList.Any())
                    {
                        SetImageList(PageRange);
                        
                    }
                    else
                    {
                        FormMessage = new FormMessage("Dem Element sind keine Images zugeordnet.", "Keine Elemente.", FormMessageType.OK | FormMessageType.Information);
                        
                    }

                }
                else
                {
                    FormMessage = new FormMessage("Die Images konnten nicht geladen werden!", "Fehler!", FormMessageType.OK | FormMessageType.Exclamation);
                }

            }
        }

        private void SetImageList(int pageRange)
        {
            var oldPageRange = PageRange;
            
            if (pageRange != PageRange)
            {
                
                PageRange = pageRange;
                PageId = IxPageStart / PageRange;
            }
            
            PageCount = ((dataCollector.ImageList.Count-1) / PageRange) + 1;
            IxPageStart = PageId > 0 ? PageId * PageRange : 0;
            var count = PageId < PageCount-1 ? PageRange : dataCollector.ImageList.Count - IxPageStart;
            IxPageLast = IxPageStart + count-1;
            ImageItems = dataCollector.ImageList.GetRange(IxPageStart, count);
        }

        private void ParseArguments()
        {
            ReferenceItem = null;
            objArgumentParsing = new clsArgumentParsing(localConfig.Globals, Environment.GetCommandLineArgs().ToList());
            if (objArgumentParsing.OList_Items != null && objArgumentParsing.OList_Items.Count == 1)
            {
                ReferenceItem = objArgumentParsing.OList_Items.First();

            }



        }

        public void NavigateInList(NavType navType)
        {
            switch (navType)
            {
                case NavType.First:
                    PageId = 0;
                    break;
                case NavType.Previous:
                    PageId--;
                    break;
                case NavType.Next:
                    PageId++;
                    break;
                case NavType.Last:
                    PageId = PageCount - 1;
                    break;
            }
            SetImageList(PageRange);
        }

        public void SetListSetResult(clsOntologyItem setResult)
        {
            if (setResult.GUID == localConfig.Globals.LState_Error.GUID)
            {
                FormMessage = new FormMessage("Die Images konnten nicht geladen werden!", "Fehler!", FormMessageType.OK | FormMessageType.Exclamation);
            }
        }

        public void ChangedPageRange(string pageRangeText)
        {
            var pageRange = 0;
            if (pageRangeText == Text_All)
            {
                pageRange = dataCollector.ImageList.Count;
            }
            else
            {
                pageRange = int.Parse(pageRangeText);
            }


            SetImageList(pageRange);

        }

        public void TryToSetWidth(string width)
        {
            int widthVal = 0;

            if (int.TryParse(width, out widthVal))
            {
                if (widthVal >= 32 && widthVal <= 256)
                {
                    Width = widthVal;
                    SetImageList(PageRange);
                }
                else
                {
                    Text_Width = Width.ToString();
                    FormMessage = new FormMessage("Bitte einen gültigen Wert wählen, mindestens 32 und maximal 256", "Ungültiger Wert", FormMessageType.Information | FormMessageType.OK);
                }
                
            }
            else
            {
                Text_Width = Width.ToString();
            }
        }

        public void TryToSetHeight(string height)
        {
            int heightVal = 0;

            if (int.TryParse(height, out heightVal))
            {
                if (heightVal >= 32 && heightVal <= 256)
                {
                    Height = heightVal;
                    SetImageList(PageRange);
                }
                else
                {
                    Text_Height = Height.ToString();
                    FormMessage = new FormMessage("Bitte einen gültigen Wert wählen, mindestens 32 und maximal 256", "Ungültiger Wert", FormMessageType.Information | FormMessageType.OK);
                }

            }
            else
            {
                Text_Height = Height.ToString();
            }
        }

        public void ChangedSelectionIndex(System.Windows.Forms.ListView.SelectedIndexCollection indizes )
        {
            selectedIndexCollection = indizes;
            IsEnabled_ToClipboard = false;
            if (indizes.Count == 1)
            {
                
                if (selectedItem != null)
                {
                    var item = ImageItems[indizes[0]].OItemImage;
                    selectedItem(item);
                }
            }

            if (indizes.Count > 0)
            {
                IsEnabled_ToClipboard = true;
            }
        }

        public void CopySelectionToClipboard()
        {
            for (var ix = 0; ix < selectedIndexCollection.Count-1; ix++)
            {
                var item = ImageItems[selectedIndexCollection[ix]].OItemImage;
                ontologyClipboard.addToClipboard(item);
            }
            
        }

        public void GetReferenceProblem()
        {
            FormMessage = new FormMessage("Bitte nur ein Element auswählen!", "Ein Element", FormMessageType.OK | FormMessageType.Information);
            Environment.Exit(0);
        }
    }
}
